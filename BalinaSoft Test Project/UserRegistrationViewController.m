//
//  UserRegistrationViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 11.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "UserRegistrationViewController.h"
#import "UserPersonalAreaViewController.h"
#import "User+Methods.h"
#import "UserType.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface UserRegistrationViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTextFied;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *customerSwitch;		

- (IBAction)registrationButtonWasPressed:(id)sender;
@end

@implementation UserRegistrationViewController

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark Actions

- (IBAction)registrationButtonWasPressed:(id)sender
{
    if ([self.loginTextFied hasText] && [self.passwordTextField hasText] && [self.nameTextField hasText]) {
        NSString *login = self.loginTextFied.text;
        NSString *pass = self.passwordTextField.text;
        NSString *name = self.nameTextField.text;
        
        UserType userType;
        if (self.customerSwitch.isOn) {
            userType = UserTypeCustomer;
        }else{
            userType = UserTypePerformer;
        }
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([User class])];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"login == %@", login];
        [request setPredicate:predicate];
        
        NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
        if (result.count) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The user exists" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }else{
            User *user = [User userWithParams:login pass:pass name:name type:@(userType)];
            [managedObjectContext save:nil];
            
            UserPersonalAreaViewController *userPersonalAreaViewController = [UserPersonalAreaViewController new];
            UIStoryboard *storyboard = self.storyboard;
            userPersonalAreaViewController = [storyboard instantiateViewControllerWithIdentifier:@USER_PERSONAL_AREA_VC_KEY];
            userPersonalAreaViewController.user = user;
            [self.navigationController pushViewController:userPersonalAreaViewController animated:YES];
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter some data" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}
@end
