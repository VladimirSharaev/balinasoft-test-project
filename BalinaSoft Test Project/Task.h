//
//  Task.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSNumber * cost;
@property (nonatomic, retain) NSString * specification;
@property (nonatomic, retain) NSDate * time;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) User *customer;
@property (nonatomic, retain) NSSet *performer;
@end

@interface Task (CoreDataGeneratedAccessors)

- (void)addPerformerObject:(User *)value;
- (void)removePerformerObject:(User *)value;
- (void)addPerformer:(NSSet *)values;
- (void)removePerformer:(NSSet *)values;

@end
