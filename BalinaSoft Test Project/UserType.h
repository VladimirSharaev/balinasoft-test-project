//
//  UserType.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 11.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#ifndef BalinaSoft_Test_Project_UserType_h
#define BalinaSoft_Test_Project_UserType_h

typedef NS_ENUM(NSUInteger, UserType) {
    UserTypeCustomer,
    UserTypePerformer,
    UserTypesCount
};

#endif
