//
//  AddCustomerTaskViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AddCustomerTaskViewController.h"
#import "AddTaskAtMapViewController.h"
#import "AppDelegate.h"
#import "Constants.h"


@interface AddCustomerTaskViewController () <AddTaskAtMapViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *costTextField;


- (IBAction)nextButtonWasPressed:(id)sender;

@end

@implementation AddCustomerTaskViewController

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark Action


- (IBAction)nextButtonWasPressed:(id)sender
{
    NSDate *dateNow = [[NSDate alloc] init];
    if ([self.titleTextField hasText] && [self.descriptionTextField hasText] && [self.addressTextField hasText] && [self.costTextField hasText]
        && (self.datePicker.date > dateNow)) {
        AddTaskAtMapViewController *addTaskAtMapViewController = [AddTaskAtMapViewController new];
        UIStoryboard *storyboard = self.storyboard;
        addTaskAtMapViewController = [storyboard instantiateViewControllerWithIdentifier:@ADD_TASK_AT_MAP_VC_KEY];
        addTaskAtMapViewController.delegate = self;
        [self.navigationController pushViewController:addTaskAtMapViewController animated:YES];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter correct values" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark AddTaskAtMapViewControllerDelegate

-(void)addCoordinateAtTask:(NSNumber *)latitude longitude:(NSNumber *)longitude
{
    NSString *title = self.titleTextField.text;
    NSString *description = self.descriptionTextField.text;
    NSDate *date = self.datePicker.date;
    NSString *address = self.addressTextField.text;
    NSNumber *cost = @([self.costTextField.text intValue]);
    [self.delegate addTask:title description:description address:address date:date cost:cost latitude:latitude longitude:longitude];
}


@end
