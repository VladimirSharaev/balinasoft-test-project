//
//  User.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic login;
@dynamic name;
@dynamic pass;
@dynamic type;
@dynamic task;

@end
