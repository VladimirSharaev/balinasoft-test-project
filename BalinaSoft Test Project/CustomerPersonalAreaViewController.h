//
//  CustomerPersonalAreaViewController.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+Methods.h"

@interface CustomerPersonalAreaViewController : UIViewController

@property (nonatomic) User *user;

@end
