//
//  TaskViewController.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task+Methods.h"
#import "UserType.h"

@protocol SubscribeTaskDelegate <NSObject>

- (void)subscribeTask:(Task *)task;

@end

@interface TaskViewController : UIViewController

@property (weak, nonatomic) id <SubscribeTaskDelegate> delegate;
@property (nonatomic) Task *task;
@property (nonatomic) UserType userType;


@end
