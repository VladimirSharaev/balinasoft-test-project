//
//  AllTasksViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AllTasksViewController.h"
#import "TaskViewController.h"
#import "AllTasksInMapViewController.h"
#import "UserType.h"
#import "AppDelegate.h"
#import "Task+Methods.h"
#import "Constants.h"


@interface AllTasksViewController () <UITableViewDataSource, UITableViewDelegate, SubscribeTaskDelegate, SubscribeTaskInMapDelegate>

@property (nonatomic) NSArray *tasks;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

-(void)reload;
- (IBAction)allTasksInMapButtonWasPressed:(id)sender;
@end

@implementation AllTasksViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self reload];
}

-(void)reload
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Task class])];
    NSArray *tasks = [managedObjectContext executeFetchRequest:request error:nil];
    self.tasks = tasks;
    
    [self.tableView reloadData];
}

#pragma mark Action

- (IBAction)allTasksInMapButtonWasPressed:(id)sender
{
    AllTasksInMapViewController *allTasksInMapViewController = [AllTasksInMapViewController new];
    UIStoryboard *storyboard = self.storyboard;
    allTasksInMapViewController = [storyboard instantiateViewControllerWithIdentifier:@ALL_TASKS_IN_MAP_VC_KEY];
    allTasksInMapViewController.tasks = self.tasks;
    allTasksInMapViewController.userType = UserTypePerformer;
    allTasksInMapViewController.delegate = self;
    [self.navigationController pushViewController:allTasksInMapViewController animated:YES];
}

#pragma mark UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tasks.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@ALL_TASKS_CELL_IDENTIFIER];
    
    Task *task = [self.tasks objectAtIndex:indexPath.row];
    NSString *title = task.title;
    NSString *cost = [task.cost stringValue];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:TAG_TITLE_LABEL];
    [titleLabel setText:title];
    UILabel *costLable = (UILabel *)[cell.contentView viewWithTag:TAG_COST_LABEL];
    [costLable setText:cost];
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
    TaskViewController *taskViewController = [TaskViewController new];
    UIStoryboard *storyboard = self.storyboard;
    taskViewController = [storyboard instantiateViewControllerWithIdentifier:@TASK_VC_KEY];
    Task *task = [self.tasks objectAtIndex:indexPath.row];
    taskViewController.task = task;
    taskViewController.userType = UserTypePerformer;
    taskViewController.delegate = self;
    [self.navigationController pushViewController:taskViewController animated:YES];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark SubscribeTaskDelegate

-(void)subscribeTask:(Task *)task
{
    [self.delegate subscribeTask:task];
}

#pragma mark SubscribeTaskInMapDelegate

-(void)subscribeTaskInMap:(Task *)task
{
    [self.delegate subscribeTask:task];
}

@end
