//
//  TaskInMapViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "TaskInMapViewController.h"
#import <MapKit/MapKit.h>

NSInteger const DistanseForRegion = 15000;

@interface TaskInMapViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation TaskInMapViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = self.taskTitle;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    
    CLLocationCoordinate2D coordinate;
    coordinate.longitude = (CLLocationDegrees)[self.longitude doubleValue];
    coordinate.latitude = (CLLocationDegrees)[self.latitude doubleValue];
    [annotation setCoordinate:coordinate];
    [annotation setTitle:self.taskTitle];
    [self.mapView addAnnotation:annotation];
    
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, DistanseForRegion, DistanseForRegion);
    [self.mapView setRegion:region animated:NO];
}

@end
