//
//  CustomerPersonalAreaViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "CustomerPersonalAreaViewController.h"
#import "AddCustomerTaskViewController.h"
#import "AllTasksInMapViewController.h"
#import "TaskViewController.h"
#import "AppDelegate.h"
#import "Task+Methods.h"

////NSString *const CellIdentifier = @"CustomerTasksTableViewCell";
////NSString *const AddCustomerTaskViewControllerKey = @"AddCustomerTaskViewController";
////NSString *const AllTasksInMapViewControllerKey = @"AllTasksInMapViewController";
////NSString *const TaskViewControllerKey = @"TaskViewController";
////NSInteger const TagTitleLabel = 1;
////NSInteger const TagCostLabel = 2;
//
//@interface CustomerPersonalAreaViewController () <UITableViewDelegate, UITableViewDataSource, AddCustomerTaskViewControllerDelegate>
//
//@property (weak, nonatomic) IBOutlet UILabel *helloLabel;
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//
//- (IBAction)AddTaskButtonWasPressed:(id)sender;
//- (IBAction)allTaskInMapButtonWasPressed:(id)sender;
//@end
//
//@implementation CustomerPersonalAreaViewController
//
//-(void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    self.helloLabel.text = [self.helloLabel.text stringByAppendingString:self.user.name];
//    
//    self.tableView.allowsMultipleSelectionDuringEditing = NO;
//}
//
//#pragma mark UITableViewDataSource
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    NSArray *tasks = [self.user.task allObjects];
//    Task *task = [tasks objectAtIndex:indexPath.row];
//    NSString *title = task.title;
//    NSString *cost = [task.cost stringValue];
//    
//    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:TagTitleLabel];
//    [titleLabel setText:title];
//    UILabel *costLable = (UILabel *)[cell.contentView viewWithTag:TagCostLabel];
//    [costLable setText:cost];
//    
//    return cell;
//}
//
//#pragma mark Action
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return self.user.task.count;
//}
//
//- (IBAction)AddTaskButtonWasPressed:(id)sender
//{
//    AddCustomerTaskViewController *addCustomerTaskViewController = [AddCustomerTaskViewController new];
//    UIStoryboard *storyboard = self.storyboard;
//    addCustomerTaskViewController = [storyboard instantiateViewControllerWithIdentifier:AddCustomerTaskViewControllerKey];
//    addCustomerTaskViewController.delegate = self;
//    [self.navigationController pushViewController:addCustomerTaskViewController animated:YES];
//}
//
//- (IBAction)allTaskInMapButtonWasPressed:(id)sender
//{
//    AllTasksInMapViewController *allTasksInMapViewController = [AllTasksInMapViewController new];
//    UIStoryboard *storyboard = self.storyboard;
//    allTasksInMapViewController = [storyboard instantiateViewControllerWithIdentifier:AllTasksInMapViewControllerKey];
//    NSArray *tasks = [self.user.task allObjects];
//    allTasksInMapViewController.tasks = tasks;
//    [self.navigationController pushViewController:allTasksInMapViewController animated:YES];
//}
//
//#pragma mark - Table View Delegate
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
//    TaskViewController *taskViewController = [TaskViewController new];
//    UIStoryboard *storyboard = self.storyboard;
//    taskViewController = [storyboard instantiateViewControllerWithIdentifier:TaskViewControllerKey];
//    NSArray *tasks = [self.user.task allObjects];
//    Task *task = [tasks objectAtIndex:indexPath.row];
//    taskViewController.task = task;
//    [self.navigationController pushViewController:taskViewController animated:YES];
//}
//
//- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//}
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        NSArray *tasks = [self.user.task allObjects];
//        Task *task = [tasks objectAtIndex:indexPath.row];
//        [self.user removeTaskObject:task];
//        
//        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        NSManagedObjectContext *context = appDelegate.managedObjectContext;
//        [context deleteObject:task];
//        [context save:nil];
//        
//        [self.tableView reloadData];
//    }
//}
//
//#pragma mark AddCustomerTaskViewControllerDelegate
//
//-(void)addTask:(NSString *)title description:(NSString *)description address:(NSString *)address date:(NSDate *)date cost:(NSNumber *)cost latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude
//{
//    Task *task = [Task taskWithParams:title description:description address:address date:date cost:cost latitude:latitude longitude:longitude];
//    task.customer = self.user;
//    [self.user addTaskObject:task];
//    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    NSManagedObjectContext *context = appDelegate.managedObjectContext;
//    [context save:nil];
//    
//    [self.tableView reloadData];
//    
//    [self.navigationController popToViewController:self animated:YES];
//}
//
//@end
