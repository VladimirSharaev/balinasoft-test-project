//
//  Task+Methods.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "Task.h"

@interface Task (Methods)

+ (Task *)taskWithParams:(NSString *)title description:(NSString *)description address:(NSString *)address date:(NSDate *)date cost:(NSNumber *)cost latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude;

@end
