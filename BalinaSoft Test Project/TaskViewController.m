//
//  TaskViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "TaskViewController.h"
#import "AppDelegate.h"
#import "TaskInMapViewController.h"
#import "User+Methods.h"
#import "UserType.h"
#import "Constants.h"

@interface TaskViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *usersLabel;
@property (weak, nonatomic) IBOutlet UIButton *subscribeButton;

- (IBAction)taskInMapButtonWasPressed:(id)sender;
- (IBAction)subscribeButtonWasPressed:(id)sender;

@end

@implementation TaskViewController

-(void)viewDidLoad
{
    self.navigationItem.title = self.task.title;
    self.descriptionLabel.text = self.task.specification;
    NSString *dateString = [NSDateFormatter localizedStringFromDate:self.task.time dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
    self.dataLabel.text = dateString;
    self.addressLabel.text = self.task.address;
    self.costLabel.text = [self.task.cost stringValue];
    self.usersLabel.text = self.task.customer.name;
    
    if (self.userType == UserTypePerformer) {
        self.subscribeButton.hidden = NO;
    }
}

#pragma mark UITableViewDataSource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@TASK_PERFOFRMER_CELL_IDENTIFIER];
    
    NSArray *performers = [self.task.performer allObjects];
    User *user = [performers objectAtIndex:indexPath.row];
    
    UILabel *performerLabel = (UILabel *)[cell.contentView viewWithTag:TAG_PERFORMER_LABEL];
    NSString *text = user.login;
    [performerLabel setText:text];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.task.performer.count;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark Action

- (IBAction)taskInMapButtonWasPressed:(id)sender
{
    TaskInMapViewController *taskInMapViewController = [TaskInMapViewController new];
    UIStoryboard *storyboard = self.storyboard;
    taskInMapViewController = [storyboard instantiateViewControllerWithIdentifier:@TASK_IN_MAP_VC_KEY];
    taskInMapViewController.latitude = self.task.latitude;
    taskInMapViewController.longitude = self.task.longitude;
    taskInMapViewController.taskTitle = self.task.title;
    [self.navigationController pushViewController:taskInMapViewController animated:YES];
}

- (IBAction)subscribeButtonWasPressed:(id)sender
{
    [self.delegate subscribeTask:self.task];
}
@end
