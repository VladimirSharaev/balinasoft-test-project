//
//  UserAuthorizationViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 11.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "UserAuthorizationViewController.h"
#import "UserRegistrationViewController.h"
#import "UserPersonalAreaViewController.h"
#import "AppDelegate.h"
#import "User+Methods.h"
#import "UserType.h"
#import "Constants.h"


@interface UserAuthorizationViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)registrationButtonWasPressed:(id)sender;
- (IBAction)loginbuttonWasPressed:(id)sender;

@end


@implementation UserAuthorizationViewController


#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark Action

- (IBAction)registrationButtonWasPressed:(id)sender
{
    UserRegistrationViewController *userRegistrationViewController = [UserRegistrationViewController new];
    UIStoryboard *storyboard = self.storyboard;
    userRegistrationViewController = [storyboard instantiateViewControllerWithIdentifier:@USER_REGISTRATION_VC_KEY];
    [self.navigationController pushViewController:userRegistrationViewController animated:YES];
}

- (IBAction)loginbuttonWasPressed:(id)sender
{
    if ([self.loginTextField hasText] && [self.passwordTextField hasText]) {
        NSString *login = self.loginTextField.text;
        NSString *pass = self.passwordTextField.text;
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([User class])];
        NSPredicate *loginPredicate = [NSPredicate predicateWithFormat: @"login == %@", login];
        [request setPredicate:loginPredicate];
        NSPredicate *passPredicate = [NSPredicate predicateWithFormat: @"pass == %@", pass];
        [request setPredicate:passPredicate];
        
        NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
        if (!result.count) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid login or password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }else{
            User *user = result.firstObject;
            UserPersonalAreaViewController *userPersonalAreaViewController = [UserPersonalAreaViewController new];
            UIStoryboard *storyboard = self.storyboard;
            userPersonalAreaViewController = [storyboard instantiateViewControllerWithIdentifier:@USER_PERSONAL_AREA_VC_KEY];
            userPersonalAreaViewController.user = user;
            [self.navigationController pushViewController:userPersonalAreaViewController animated:YES];
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter some data" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}
@end
