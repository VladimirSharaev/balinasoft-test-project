//
//  Constants.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#ifndef BalinaSoft_Test_Project_Constants_h
#define BalinaSoft_Test_Project_Constants_h

#define USER_REGISTRATION_VC_KEY "UserRegistrationViewController"
#define USER_PERSONAL_AREA_VC_KEY "UserPersonalAreaViewController"
#define USER_TASKS_TABLE_VIEW_CELL_INDENTIFIER "UserTasksTableViewCell"
#define ADD_CUSTOMER_TASK_VC_KEY "AddCustomerTaskViewController"
#define ALL_TASKS_IN_MAP_VC_KEY "AllTasksInMapViewController"
#define ALL_TASKS_VC_KEY "AllTasksViewController"
#define TASK_VC_KEY "TaskViewController"
#define TAG_TITLE_LABEL 1
#define TAG_COST_LABEL 2
#define ADD_TASK_AT_MAP_VC_KEY "AddTaskAtMapViewController"
#define TASK_PERFOFRMER_CELL_IDENTIFIER "TaskPerformerCell"
#define TASK_IN_MAP_VC_KEY "TaskInMapViewController"
#define TAG_PERFORMER_LABEL 1
#define ALL_TASKS_CELL_IDENTIFIER "AllTasksCell"



#endif
