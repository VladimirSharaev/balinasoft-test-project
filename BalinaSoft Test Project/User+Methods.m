//
//  User+Methods.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User+Methods.h"
#import "AppDelegate.h"

@implementation User (Methods)

+ (User *)userWithParams:(NSString *)login pass:(NSString *)pass name:(NSString *)name type:(NSNumber *)type
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    User *user = (User *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:context];
    if (user) {
        user.login = login;
        user.pass = pass;
        user.name = name;
        user.type = type;
    }
    return user;
}

@end
