//
//  User+Methods.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User.h"

@interface User (Methods)

+ (User *)userWithParams:(NSString *)login pass:(NSString *)pass name:(NSString *)name type:(NSNumber *)type;

@end
