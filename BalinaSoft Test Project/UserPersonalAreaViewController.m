//
//  UserPersonalAreaViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "UserPersonalAreaViewController.h"
#import "AddCustomerTaskViewController.h"
#import "AllTasksViewController.h"
#import "AllTasksInMapViewController.h"
#import "TaskViewController.h"
#import "AppDelegate.h"
#import "Task+Methods.h"
#import "UserType.h"
#import "Constants.h"

@interface UserPersonalAreaViewController () <UITableViewDelegate, UITableViewDataSource, AddCustomerTaskViewControllerDelegate, SubscribeTaskInAllTasksDelegate>

@property (weak, nonatomic) IBOutlet UILabel *helloLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)AddTaskButtonWasPressed:(id)sender;
- (IBAction)allTaskInMapButtonWasPressed:(id)sender;
@end

@implementation UserPersonalAreaViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.helloLabel.text = [self.helloLabel.text stringByAppendingString:self.user.name];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

#pragma mark Action

- (IBAction)AddTaskButtonWasPressed:(id)sender
{
    UserType userType = [self.user.type integerValue];
    if (userType == UserTypeCustomer) {
        AddCustomerTaskViewController *addCustomerTaskViewController = [AddCustomerTaskViewController new];
        UIStoryboard *storyboard = self.storyboard;
        addCustomerTaskViewController = [storyboard instantiateViewControllerWithIdentifier:@ADD_CUSTOMER_TASK_VC_KEY];
        addCustomerTaskViewController.delegate = self;
        [self.navigationController pushViewController:addCustomerTaskViewController animated:YES];
    }else{
        AllTasksViewController *allTasksViewController = [AllTasksViewController new];
        UIStoryboard *storyboard = self.storyboard;
        allTasksViewController = [storyboard instantiateViewControllerWithIdentifier:@ALL_TASKS_VC_KEY];
        allTasksViewController.delegate = self;
        [self.navigationController pushViewController:allTasksViewController animated:YES];
    }
}

- (IBAction)allTaskInMapButtonWasPressed:(id)sender
{
    AllTasksInMapViewController *allTasksInMapViewController = [AllTasksInMapViewController new];
    UIStoryboard *storyboard = self.storyboard;
    allTasksInMapViewController = [storyboard instantiateViewControllerWithIdentifier:@ALL_TASKS_IN_MAP_VC_KEY];
    NSArray *tasks = [self.user.task allObjects];
    allTasksInMapViewController.tasks = tasks;
    [self.navigationController pushViewController:allTasksInMapViewController animated:YES];
}



#pragma mark UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.user.task.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@USER_TASKS_TABLE_VIEW_CELL_INDENTIFIER];
    
    NSArray *tasks = [self.user.task allObjects];
    Task *task = [tasks objectAtIndex:indexPath.row];
    NSString *title = task.title;
    NSString *cost = [task.cost stringValue];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:TAG_TITLE_LABEL];
    [titleLabel setText:title];
    UILabel *costLable = (UILabel *)[cell.contentView viewWithTag:TAG_COST_LABEL];
    [costLable setText:cost];
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
    
    TaskViewController *taskViewController = [TaskViewController new];
    UIStoryboard *storyboard = self.storyboard;
    taskViewController = [storyboard instantiateViewControllerWithIdentifier:@TASK_VC_KEY];
    NSArray *tasks = [self.user.task allObjects];
    Task *task = [tasks objectAtIndex:indexPath.row];
    taskViewController.task = task;
    [self.navigationController pushViewController:taskViewController animated:YES];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        
        NSArray *tasks = [self.user.task allObjects];
        Task *task = [tasks objectAtIndex:indexPath.row];
        
        UserType userType = [self.user.type integerValue];
        if (userType == UserTypeCustomer) {
            [self.user removeTaskObject:task];
        
            [context deleteObject:task];
            [context save:nil];
        
            [self.tableView reloadData];
        }else{
            [self.user removeTaskObject:task];
            [task removePerformerObject:self.user];
            
            if (![context save:nil]) {
                NSLog(@"Whoops");
            }
            [self.tableView reloadData];
        }
    }
}

#pragma mark AddCustomerTaskViewControllerDelegate

-(void)addTask:(NSString *)title description:(NSString *)description address:(NSString *)address date:(NSDate *)date cost:(NSNumber *)cost latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude
{
    Task *task = [Task taskWithParams:title description:description address:address date:date cost:cost latitude:latitude longitude:longitude];
    task.customer = self.user;
    [self.user addTaskObject:task];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    [context save:nil];
    
    [self.tableView reloadData];
    
    [self.navigationController popToViewController:self animated:YES];
}

#pragma  mark SubscribeTaskInAllTasksDelegate

-(void)subscribeTask:(Task *)task
{
    [self.user addTaskObject:task];
    [task addPerformerObject:self.user];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    [context save:nil];
    
    [self.tableView reloadData];
    [self.navigationController popToViewController:self animated:YES];
}

@end
