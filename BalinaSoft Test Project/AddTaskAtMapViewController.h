//
//  AddTaskAtMapViewController.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddTaskAtMapViewControllerDelegate <NSObject>

- (void)addCoordinateAtTask:(NSNumber *)latitude longitude:(NSNumber *)longitude;

@end

@interface AddTaskAtMapViewController : UIViewController

@property (weak, nonatomic) id <AddTaskAtMapViewControllerDelegate> delegate;

@end
