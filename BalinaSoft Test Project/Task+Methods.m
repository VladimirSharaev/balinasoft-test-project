//
//  Task+Methods.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "Task+Methods.h"
#import "AppDelegate.h"

@implementation Task (Methods)

+ (Task *)taskWithParams:(NSString *)title description:(NSString *)description address:(NSString *)address date:(NSDate *)date cost:(NSNumber *)cost latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    Task *task = (Task *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:context];
    if (task) {
        task.title = title;
        task.specification = description;
        task.address = address;
        task.time = date;
        task.cost = cost;
        task.latitude = latitude;
        task.longitude = longitude;
    }
    return task;
}

@end
