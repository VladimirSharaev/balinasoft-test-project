//
//  User.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * pass;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSSet *task;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addTaskObject:(NSManagedObject *)value;
- (void)removeTaskObject:(NSManagedObject *)value;
- (void)addTask:(NSSet *)values;
- (void)removeTask:(NSSet *)values;

@end
