//
//  Task.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "Task.h"
#import "User.h"


@implementation Task

@dynamic cost;
@dynamic specification;
@dynamic time;
@dynamic title;
@dynamic latitude;
@dynamic longitude;
@dynamic address;
@dynamic customer;
@dynamic performer;

@end
