//
//  AllTasksInMapViewController.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserType.h"
#import "Task+Methods.h"

@protocol SubscribeTaskInMapDelegate <NSObject>

- (void)subscribeTaskInMap:(Task *)task;

@end

@interface AllTasksInMapViewController : UIViewController

@property (weak, nonatomic) id <SubscribeTaskInMapDelegate> delegate;

@property (nonatomic) NSArray *tasks;
@property (nonatomic) UserType userType;

@end
