//
//  AllTasksInMapViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AllTasksInMapViewController.h"
#import <MapKit/MapKit.h>
#import "AppDelegate.h"
#import "Task+Methods.h"
#import "TaskViewController.h"
#import "Constants.h"

NSString *const TitleString = @"All tasks";

@interface AllTasksInMapViewController () <MKMapViewDelegate, SubscribeTaskDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end

@implementation AllTasksInMapViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = TitleString;
    
    for (Task *task in self.tasks) {
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        
        CLLocationCoordinate2D coordinate;
        coordinate.longitude = (CLLocationDegrees)[task.longitude doubleValue];
        coordinate.latitude = (CLLocationDegrees)[task.latitude doubleValue];
        [annotation setCoordinate:coordinate];
        [annotation setTitle:task.title];
        [self.mapView addAnnotation:annotation];
    }
}

#pragma mark MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    MKPointAnnotation *annotation = [view annotation];
    NSString *title = annotation.title;
    NSNumber *latitude = @(annotation.coordinate.latitude);
    NSNumber *longitude = @(annotation.coordinate.longitude);
    Task *thisTask = nil;
    for (Task *task in self.tasks) {
        if ([task.title isEqualToString:title] && [task.latitude isEqualToNumber:latitude] && [task.longitude isEqualToNumber:longitude]) {
            thisTask = task;
            break;
        }
    }
    
    if (thisTask != nil) {
        TaskViewController *taskViewController = [TaskViewController new];
        UIStoryboard *storyboard = self.storyboard;
        taskViewController = [storyboard instantiateViewControllerWithIdentifier:@TASK_VC_KEY];
        taskViewController.task = thisTask;
        taskViewController.userType = self.userType;
        taskViewController.delegate = self;
        [self.navigationController pushViewController:taskViewController animated:YES];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
            pinView.tintColor = MKPinAnnotationColorRed;
            pinView.enabled = YES;
            pinView.canShowCallout = YES;
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

            pinView.rightCalloutAccessoryView = btn;
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}

#pragma mark SubscribeTaskDelegate

-(void)subscribeTask:(Task *)task
{
    [self.delegate subscribeTaskInMap:task];
}

@end
