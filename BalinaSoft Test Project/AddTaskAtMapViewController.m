//
//  AddTaskAtMapViewController.m
//  BalinaSoft Test Project
//
//  Created by Vladimir on 15.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AddTaskAtMapViewController.h"
#import <MapKit/MapKit.h>
#import "CustomerPersonalAreaViewController.h"

NSString *const AnnotationTitle = @"This plase?";

@interface AddTaskAtMapViewController ()

@property (nonatomic) MKPointAnnotation *annotation;
@property (nonatomic) NSNumber *latitude;
@property (nonatomic) NSNumber *longitude;
@property (nonatomic) BOOL canAddCoordinate;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)doneButtonWasPressed:(id)sender;

@end

@implementation AddTaskAtMapViewController

-(void)viewDidLoad
{
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    longPressGestureRecognizer.minimumPressDuration = 2.0;
    [self.mapView addGestureRecognizer:longPressGestureRecognizer];
    
    self.canAddCoordinate = NO;
    self.annotation = [[MKPointAnnotation alloc] init];
}

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    self.latitude = @(touchMapCoordinate.latitude);
    self.longitude = @(touchMapCoordinate.longitude);
   
    [self.annotation setCoordinate:touchMapCoordinate];
    [self.annotation setTitle:AnnotationTitle];
    [self.mapView addAnnotation:self.annotation];
    
    self.canAddCoordinate = YES;
}

- (IBAction)doneButtonWasPressed:(id)sender
{
    if (self.canAddCoordinate
        ) {
        [self.delegate addCoordinateAtTask:self.latitude longitude:self.longitude];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Plase pin at map" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}
@end
