//
//  AllTasksViewController.h
//  BalinaSoft Test Project
//
//  Created by Vladimir on 16.06.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task+Methods.h"

@protocol SubscribeTaskInAllTasksDelegate <NSObject>

- (void)subscribeTask:(Task *)task;

@end

@interface AllTasksViewController : UIViewController

@property (weak, nonatomic) id <SubscribeTaskInAllTasksDelegate> delegate;

@end
